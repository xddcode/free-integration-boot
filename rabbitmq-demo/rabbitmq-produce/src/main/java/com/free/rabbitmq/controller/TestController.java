package com.free.rabbitmq.controller;

import com.free.common.utils.R;
import com.free.rabbitmq.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rabbitmq")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/putStorage")
    public R putStorage() {
        if (testService.putStorage()) {
            return R.succeed("入库成功");
        }
        return R.failed("入库失败");
    }
}
