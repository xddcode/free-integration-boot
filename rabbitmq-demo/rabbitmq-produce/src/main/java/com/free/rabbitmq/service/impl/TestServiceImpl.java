package com.free.rabbitmq.service.impl;

import com.free.rabbitmq.common.model.MsgMail;
import com.free.rabbitmq.common.template.MqTemplate;
import com.free.rabbitmq.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class TestServiceImpl implements TestService{

    @Resource
    private MqTemplate mqTemplate;

    @Override
    public boolean putStorage() {
        log.info("===模拟入库业务入库成功===");

        log.info("===开始发送邮件通知===");
        MsgMail mail = new MsgMail();
        mail.setPersonName("丁浩");
        mail.setSubject("测试邮件");
        mail.setTo("xxxxx@qq.com");
        mqTemplate.sendMail(mail);
        return true;
    }

}
