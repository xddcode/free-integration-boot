package com.free.rabbitmq.common.constant;


public class RabbitConstant {

    public interface ExchangeQueues {
        // 发送邮件
        String MAIL_QUEUE_NAME = "mail.queue";

        String MAIL_EXCHANGE_NAME = "mail.exchange";

        String MAIL_ROUTING_KEY_NAME = "mail.routing.key";

        //发短信
        String SMS_QUEUE_NAME = "sms.queue";

        String SMS_EXCHANGE_NAME = "sms.exchange";

        String SMS_ROUTING_KEY_NAME = "sms.routing.key";

        //推送
        String PUSH_QUEUE_NAME = "push.queue";

        String PUSH_EXCHANGE_NAME = "push.exchange";

        String PUSH_ROUTING_KEY_NAME = "push.routing.key";
    }

    public interface MsgLogStatus {
        // 消息投递中
        Integer DELIVERING = 0;
        // 投递成功
        Integer DELIVER_SUCCESS = 1;
        // 投递失败
        Integer DELIVER_FAIL = 2;
        // 已消费
        Integer CONSUMED_SUCCESS = 3;
    }
}
