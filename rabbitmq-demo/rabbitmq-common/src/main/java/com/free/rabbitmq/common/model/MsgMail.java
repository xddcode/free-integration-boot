package com.free.rabbitmq.common.model;

import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * 短信发送实体
 *
 * @author dinghao
 * @date 2020/5/14
 */
@Data
public class MsgMail implements Serializable {

    private static final long serialVersionUID = 5896433785934547511L;

    /**
     * 消息id
     */
    private String msgId;

    /**
     * 接受的邮件地址
     */
    private String to;

    /**
     * 主题
     */
    private String subject;

    /**
     * 附件文件名称
     */
    private String fileName;

    /**
     *  附件文件流
     */
    private ByteArrayOutputStream baos;

    /**
     *  附件文件字节数组
     */
    private byte[] byteArray;

    /**
     * 接收人姓名
     */
    private String personName;

}
