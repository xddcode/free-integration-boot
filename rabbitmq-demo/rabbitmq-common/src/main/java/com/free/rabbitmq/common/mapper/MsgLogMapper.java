package com.free.rabbitmq.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.free.rabbitmq.common.model.MsgLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MsgLogMapper extends BaseMapper<MsgLog> {
}
