package com.free.rabbitmq.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.free.rabbitmq.common.model.MsgLog;

import java.util.Date;
import java.util.List;

public interface MsgLogService extends IService<MsgLog> {

    MsgLog selectByMsgId(String msgId);

    List<MsgLog> selectTimeoutMsg();

    boolean insertMsg(MsgLog msgLog);

    boolean updateStatus(String msgId, Integer status);

    boolean updateTryCount(String msgId, Integer tryCount, Date tryTime);
}
