package com.free.rabbitmq.common.model;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.free.rabbitmq.common.constant.RabbitConstant;
import com.free.rabbitmq.common.util.JodaTimeUtil;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("msg_log")
public class MsgLog extends Model<MsgLog> {

    private static final long serialVersionUID = 1486107113278021447L;

    @TableId(type = IdType.INPUT)
    private String msgId;

    private String msg;

    private String exchange;

    private String routingKey;

    private Integer status;

    private Integer tryCount;

    private Date nextTryTime;

    private Date createTime;

    private Date updateTime;

    public MsgLog(String msgId, Object msg, String exchange, String routingKey) {
        this.msgId = msgId;
        this.msg = JSON.toJSONString(msg);
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.status = RabbitConstant.MsgLogStatus.DELIVERING;
        this.tryCount = 0;
        Date date = new Date();
        this.nextTryTime = (JodaTimeUtil.plusMinutes(date, 1));
    }
}
