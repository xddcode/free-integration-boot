package com.free.rabbitmq.service.impl;

import com.free.rabbitmq.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MailServiceImpl implements MailService {

    @Override
    public boolean sendMail() {
        log.info("====邮件发送成功====");
        return true;
    }
}
