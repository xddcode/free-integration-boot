package com.free.rabbitmq.consumer;

import com.free.rabbitmq.proxy.BaseConsumer;
import com.free.rabbitmq.common.util.MessageHelper;
import com.free.rabbitmq.common.model.MsgMail;
import com.free.rabbitmq.service.MailService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MailConsumer implements BaseConsumer {

    @Autowired
    private MailService mailService;

    @Override
    public void consume(Message message, Channel channel) {
       // MsgMail mail = MessageHelper.msgToObj(message, MsgMail.class);
      //  log.info("收到消息: {}", mail.toString());
        boolean success = mailService.sendMail();
        if (!success) {
            log.error("=== MailConsumer.consume： 邮件发送失败 ===");
        }
    }

}

