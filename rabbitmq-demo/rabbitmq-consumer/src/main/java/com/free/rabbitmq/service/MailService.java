package com.free.rabbitmq.service;

public interface MailService {

    boolean sendMail();
}
